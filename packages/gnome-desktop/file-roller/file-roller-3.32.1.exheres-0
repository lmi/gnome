# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Purpose License v2

require file-roller
require meson

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    archive [[ description = [ add support for archive formats that libarchive supports ] ]]
    libnotify [[ description = [ display completion notifications ] ]]
    nautilus-extension [[ description = [ build nautilus context menu actions ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        core/json-glib[>=0.14.0]
        dev-libs/glib:2[>=2.36.0]
        virtual/cpio
        x11-libs/gtk+:3[>=3.13.2]
        archive? ( app-arch/libarchive[>=3.0.0] )
        libnotify? ( x11-libs/libnotify[>=0.4.3] )
        nautilus-extension? ( gnome-desktop/nautilus[>=2.22.2] )
    suggestion:
        app-arch/unzip [[
            description = [ ${PN} can only use InfoZIP unzip to extract .zip files ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Drun-in-place=false
    -Dnautilus-actions=false
    -Dpackagekit=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'archive libarchive'
    'libnotify notification'
    'nautilus-extension nautilus-actions'
)

