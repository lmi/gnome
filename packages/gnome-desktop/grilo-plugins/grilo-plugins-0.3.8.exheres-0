# Copyright 2013-2014 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="A collection of plugins for the Grilo framework"
HOMEPAGE="https://wiki.gnome.org/Grilo"

LICENCES="LGPL-2.1"
SLOT="0.3"
PLATFORMS="~amd64"
MYOPTIONS="
    bookmarks
    chromaprint
    dmap
    freebox [[ description = [ Add support for Freebox (French TV Service) ] ]]
    thetvdb
    tracker
"

DEPENDENCIES="
    build:
        dev-util/gperf
        sys-devel/gettext
    build+run:
        app-arch/libarchive
        core/json-glib
        dev-db/sqlite:3
        dev-libs/glib:2[>=2.44]
        dev-libs/liboauth
        dev-libs/libxml2:2.0
        media-libs/libmediaart:2.0
        gnome-desktop/gnome-online-accounts[>=3.17.91] [[ note = automagic ]]
        gnome-desktop/grilo:0.3[>=0.3.6]
        gnome-desktop/libgdata[>=0.9.1]
        gnome-desktop/libsoup:2.4
        gnome-desktop/totem-pl-parser[>=3.4.1]
        gnome-desktop/yelp-tools
        bookmarks? ( dev-libs/gom:1.0[>=0.3.2] )
        chromaprint? ( media-libs/gstreamer:1.0 )
        dmap? ( media-libs/libdmapsharing:3.0[>=2.9.12] )
        freebox? ( net-dns/avahi[gtk3] )
        thetvdb? ( dev-libs/gom:1.0[>=0.3.2] )
        tracker? ( app-pim/tracker:=[>=0.10.5] )
"

BUGS_TO="keruspe@exherbo.org"

MESON_SRC_CONFIGURE_PARAMS=(
    -Denable-chromaprint=yes
    -Denable-dleyna=yes
    -Denable-filesystem=yes
    -Denable-flickr=yes
    -Denable-gravatar=yes
    -Denable-jamendo=yes
    -Denable-local-metadata=yes
    -Denable-magnatune=yes
    -Denable-metadata-store=yes
    -Denable-opensubtitles=yes
    -Denable-optical-media=yes
    -Denable-podcasts=yes
    -Denable-raitv=yes
    -Denable-shoutcast=yes
    -Denable-tmdb=yes
    -Denable-video=yes
    -Denable-youtube=yes

    -Denable-lua-factory=no
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bookmarks enable-bookmarks yes no'
    'chromaprint enable-chromaprint yes no'
    'dmap enable-dmap yes no'
    'freebox enable-freebox yes no'
    'thetvdb enable-thetvdb yes no'
    'tracker enable-tracker yes no'
)

