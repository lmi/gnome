# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require webkit [ required_gcc='4.7' ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.14 ] ]

SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="debug [[ description = [ debug build of webkit (not recommended for daily usage) ] ]]
    gtk-doc
    gobject-introspection
    media [[ description = [ HTML5 video/audio support ] ]]
    spell
    opengl [[ description = [ enable accelerated rendering via OpenGL (enables WebGL) ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: ar as bg cs de el en_CA en_GB eo es et eu fr gl gu he hi hu id it kn ko lt lv ml mr
               nb nl or pa pl pt pt_BR ro ru sl sr sr@latin sv ta te uk vi zh_CN )
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-lang/python:*
        dev-lang/ruby:*
        sys-devel/bison
        dev-util/gperf
        sys-devel/flex[>=2.5.33]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
    build+run:
        media-libs/libpng:=[>=1.2]
        media-libs/libwebp
        dev-libs/glib:2[>=2.36.0]
        dev-libs/icu:=
        sys-libs/zlib
        dev-libs/libxml2:2.0[>=2.6]
        x11-libs/pango[>=1.32.0]
        x11-libs/cairo[>=1.10][X] [[ note = [ X requied for cairo-gl for css support ] ]]
        x11-libs/gtk+:3[>=3.10.0][gobject-introspection?]
        x11-libs/libXt
        x11-dri/mesa
        gnome-desktop/libsoup:2.4[>=2.42.0][gobject-introspection?]
        dev-libs/libsecret:1
        media-libs/fontconfig[>=2.5]
        media-libs/freetype:2
        x11-libs/harfbuzz[>=0.9.7]
        dev-db/sqlite:3[>=3.0]
        dev-libs/libxslt[>=1.1.7]
        gps/geoclue:2.0
        x11-libs/libXrender
        gnome-desktop/libgudev
        x11-libs/gtk+:2[>=2.24.10] [[ note = [ required for plugin process ] ]]
        dev-libs/at-spi2-core[>=2.6.2]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.32.0] )
        media? ( media-libs/gstreamer:1.0[>=1.0.3]
                 media-plugins/gst-plugins-base:1.0[>=1.0.3] )
        opengl? ( x11-libs/libXcomposite
                  x11-libs/libXdamage ) [[
            *note = [ required for opengl acceleration backend ]
        ]]
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        spell? ( app-spell/enchant:0[>=0.22] )
"

RESTRICT="test" # requires X

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2.4.7-GMutexLocker.patch
    "${FILES}"/${PN}-2.4.8-pkg-config-path.patch
    "${FILES}"/${PN}-2.4.6-fix-disable-accelerated-compositing.patch
    "${FILES}"/${PN}-2.4.8-gcc5.patch
)

# TODO(compnerd) enable video-track
DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-webkit1'
    '--enable-webkit2'

    '--with-gtk=3.0'
    '--enable-x11-target'
    '--disable-wayland-target'
    '--enable-glx'
    '--disable-egl'
    '--disable-gles2'

    # NOTE(compnerd) libsecret is already present
    '--enable-credential-storage'

    '--enable-css-filters'

    '--enable-geolocation'

    '--enable-svg'
    '--enable-svg-fonts'

    # NOTE(compnerd) until non-Linux kernels are supported, unconditionally enable gamepad
    '--enable-gamepad'

    # NOTE(compnerd) until we enable arm/s390/ppc/ppc64, unconditionally enable JIT
    '--enable-jit'

    # TODO(compnerd) should this be controlled via the debug flag?
    '--disable-coverage'

    # TODO(compnerd) port to upower:1 APIs
    '--disable-battery-status'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'opengl accelerated-compositing'
    'opengl glx'
    'opengl webgl'
    'debug'
    'debug debug-symbols'
    'debug opcode-stats'
    '!debug optimizations'
    '!debug fast-malloc'
    'gtk-doc'
    'gobject-introspection introspection'
    'media video'
    'media web-audio'
    'spell spellcheck'
)
WORK="${WORKBASE}/webkitgtk-${PV}/"

src_prepare() {
    webkit_src_prepare

    eautoreconf
}

src_compile() {
    edo mkdir -p "${WORK}/DerivedSources/webkit"                        \
                 "${WORK}/DerivedSources/webkitdom"                     \
                 "${WORK}/DerivedSources/WebCore"                       \
                 "${WORK}/DerivedSources/ANGLE"                         \
                 "${WORK}/DerivedSources/Platform"                      \
                 "${WORK}/DerivedSources/WebKit2/webkit2gtk/webkit2"    \
                 "${WORK}/DerivedSources/InjectedBundle"
    default
}

